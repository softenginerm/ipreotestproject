﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using StringReplacement.Services;

namespace StringReplacement.Controllers
{
    public class TermsController : ApiController
    {
        private readonly IServiceData dataService;
        public TermsController(IServiceData dataService)
        {
            this.dataService = dataService;
        }
        
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/terms/replace")]
        public IHttpActionResult ReplcaeData([FromBody] Models.CallingObject replacementobject)
        {
            var oldvalue = replacementobject.OldValue;
            var newvalue = replacementobject.NewValue;
            var txtBlurb = replacementobject.TextBlurb;
            var changedvalue = this.dataService.ReplaceData(txtBlurb, oldvalue, newvalue);
            return Ok(changedvalue);
        }

}
}
