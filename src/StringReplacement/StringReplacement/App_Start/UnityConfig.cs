using System.Web.Mvc;
using StringReplacement.Services;
using Unity;
using Unity.Mvc5;

namespace StringReplacement
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
           container.RegisterType<IServiceData, ServiceData>();
            
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}