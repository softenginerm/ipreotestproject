﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace StringReplacement.Services
{
    public class ServiceData :IServiceData
    {
        public string ReplaceData(string textblurb, string oldvalue, string newvalue)
        {
            var result = ReplaceEx(textblurb, oldvalue, newvalue);
            return result;
        }


        private static string ReplaceEx(string original, string pattern, string replacement)
        {
            int searchStartPos, patetrnMatchFirstPos;
            var count = searchStartPos = 0;

            int inc = (original.Length / pattern.Length) * (replacement.Length - pattern.Length);

            char[] chars = new char[original.Length + Math.Max(0, inc)];

            while ((patetrnMatchFirstPos = original.IndexOf(pattern, searchStartPos, StringComparison.Ordinal)) != -1)
            {
                for (int i = searchStartPos; i < patetrnMatchFirstPos; ++i)
                    chars[count++] = original[i];
                
                if (patetrnMatchFirstPos == 0||(patetrnMatchFirstPos+pattern.Length+1 == original.Length) ||(patetrnMatchFirstPos > 0 && original[patetrnMatchFirstPos - 1] == ' ' && original[patetrnMatchFirstPos + pattern.Length] == ' '))
                {
                    foreach (char t in replacement)
                        chars[count++] = t;
                }
                else
                {
                    chars[count++] = original[patetrnMatchFirstPos];
                }
                searchStartPos = patetrnMatchFirstPos + pattern.Length;
            }

            if (searchStartPos == 0) return original;
            for (int i = searchStartPos; i < original.Length; ++i)
                chars[count++] = original[i];

            return new string(chars, 0, count);
        }


    }
}