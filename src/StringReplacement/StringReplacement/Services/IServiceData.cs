﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringReplacement.Services
{
    public interface IServiceData
    {
        string ReplaceData(string textblurb, string oldvalue, string newvalue);
    }
}
