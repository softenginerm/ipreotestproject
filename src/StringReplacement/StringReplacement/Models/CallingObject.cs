﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StringReplacement.Models
{
    public class CallingObject
    {
        public string TextBlurb { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
    }
}