﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringReplacement.Models;
using StringReplacement.Services;

namespace StringReplacement.Test
{
    
    public class ServiceDataTestContext
    {
        private readonly IServiceData service;
        private string returnedValue;
        public ServiceDataTestContext()
        {
            service = new ServiceData();
        }

        public ServiceDataTestContext WhenAServiceCallIsMade(CallingObject dataCallingObject)
        {
            var textBlurb = dataCallingObject.TextBlurb;
            var oldValue = dataCallingObject.OldValue;
            var newvalue = dataCallingObject.NewValue;
            returnedValue = service.ReplaceData(textBlurb, oldValue, newvalue);
            return this;
        }

        public ServiceDataTestContext ThenOnlyWholeWordChanges()
        {
            Assert.AreEqual("Some request to replace Super ( as Super) not Test or RASHEDTEst",this.returnedValue);
            return this;
        }

        public ServiceDataTestContext ThenNoWordChanges()
        {
            Assert.AreEqual("Some request to replace Test ( as Super) not Test or RASHEDTEst given that only lowercase TEST is looked At", this.returnedValue);
            return this;
        }


        public ServiceDataTestContext ThenDigitChanges()
        {
            Assert.AreEqual("Replace 8.99 ( as 8.99) not 99 or 2362429 given that only \"9\" is looked At", this.returnedValue);
            return this;
        }

        public ServiceDataTestContext ThenBeginingChangesMadeSuccessfully()
        {
            Assert.AreEqual("Super Some request to replace Test", this.returnedValue);
            return this;
        }

        public ServiceDataTestContext ThenEndChangesMadeSuccessfully()
        {
            Assert.AreEqual("Some request to replace Test but its not small TEST thus not been changed.\nBut this last wil change Super.", this.returnedValue);
            return this;
        }
    }
}
