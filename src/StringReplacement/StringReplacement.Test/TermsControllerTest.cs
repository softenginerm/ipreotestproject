﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringReplacement.Models;

namespace StringReplacement.Test
{
    [TestClass]
    public class TermsControllerTest
    {
        [TestMethod]
        public void ReplaceShouldReturnCorrectValue()
        {
            CallingObject data = new CallingObject(){TextBlurb = "This is a test", OldValue = "test", NewValue = "SuperTest"};

            new TermsControllerTestContext()
                .WhenReplaceIsCalled(data)
                .ThenStringReturned();
        }
    }
}
