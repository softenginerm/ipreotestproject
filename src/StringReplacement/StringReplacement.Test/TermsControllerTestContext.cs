﻿using System;
using System.Web.Http.Results;
using StringReplacement.Controllers;
using StringReplacement.Services;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringReplacement.Models;

namespace StringReplacement.Test
{
    public class TermsControllerTestContext :IDisposable
    {
        private readonly TermsController ctrler;
        private readonly IServiceData dataService;
        private readonly string oldValue;
        private readonly string newValue;
        private string chnagedValue;
        public TermsControllerTestContext()
        {
            this.dataService = A.Fake<IServiceData>(s => s.Strict());
            this.ctrler = new TermsController(this.dataService);
            this.oldValue = "test";
            this.newValue = "SuperTest";
            this.chnagedValue = "This is a SuperTest";

        }

        public TermsControllerTestContext WhenReplaceIsCalled(CallingObject data1)
        {
            var old = data1.OldValue;
            var newval = data1.NewValue;
            var txtb = data1.TextBlurb;
            A.CallTo(() => this.dataService.ReplaceData(txtb, old, newval))
                .Returns(this.chnagedValue);

            if (this.ctrler.ReplcaeData(data1) is OkNegotiatedContentResult<string> returneddata)
            {
                this.chnagedValue = returneddata.Content;
            }
            return this;
        }

        public TermsControllerTestContext ThenStringReturned()
        {
            Assert.AreEqual("This is a SuperTest", this.chnagedValue);
            return this;
        }
        public void Dispose()
        {
            this.Dispose(true);
            System.GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                ctrler?.Dispose();
            }
        }
    }
}
