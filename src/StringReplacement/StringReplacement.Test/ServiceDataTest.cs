﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringReplacement.Models;
using StringReplacement.Services;

namespace StringReplacement.Test
{
    [TestClass]
    public class ServiceDataTest
    {
        [TestMethod]
        public void ReplaceWholeWordOnly()
        {
            CallingObject strR =new CallingObject();
            strR.TextBlurb = "Some request to replace test ( as Super) not Test or RASHEDTEst";
            strR.OldValue = "test";
            strR.NewValue = "Super";
            new ServiceDataTestContext()
                .WhenAServiceCallIsMade(strR)
                .ThenOnlyWholeWordChanges();

        }

        [TestMethod]
        public void TestNoDataBeenChangedWhenNoWordMatchFound()
        {
           CallingObject strR = new CallingObject();
            strR.TextBlurb = "Some request to replace Test ( as Super) not Test or RASHEDTEst given that only lowercase TEST is looked At";
            strR.OldValue = "test";
            strR.NewValue = "Super";
            new ServiceDataTestContext()
                .WhenAServiceCallIsMade(strR)
                .ThenNoWordChanges();

        }
        
        [TestMethod]
        public void TestDigitValueChangeSuccessfully()
        {
            CallingObject strR = new CallingObject();
            strR.TextBlurb = "Replace 9 ( as 8.99) not 99 or 2362429 given that only \"9\" is looked At";
            strR.OldValue = "9";
            strR.NewValue = "8.99";
            new ServiceDataTestContext()
                .WhenAServiceCallIsMade(strR)
                .ThenDigitChanges();

        }

        [TestMethod]
        public void TestBegginingDataChangesSuccessfully()
        {
            CallingObject strR = new CallingObject();
            strR.TextBlurb = "test Some request to replace Test";
            strR.OldValue = "test";
            strR.NewValue = "Super";
            new ServiceDataTestContext()
                .WhenAServiceCallIsMade(strR)
                .ThenBeginingChangesMadeSuccessfully();

        }

        [TestMethod]
        public void TestEndDataChangesSuccessfully()
        {
            CallingObject strR = new CallingObject();
            strR.TextBlurb = "Some request to replace Test but its not small TEST thus not been changed.\nBut this last wil change test.";
            strR.OldValue = "test";
            strR.NewValue = "Super";
            new ServiceDataTestContext()
                .WhenAServiceCallIsMade(strR)
                .ThenEndChangesMadeSuccessfully();

        }
    }
}
